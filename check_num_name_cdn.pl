
use strict;
use Data::Dumper;

my $cust = require 'cust.pl'; 
#say Dumper $cust;

my @checknum;
my @checkname;
foreach my $a (@{$cust}) {

unless($a->{num} =~ /^\d{5}$/) {print STDERR "ERROR: There isn't number or it has wrong format:$a->{num}_$a->{name}\n";}
unless($a->{name} =~ /^\w+$/ || !$a->{name} =~ /[-\/]/) {print STDERR "ERROR: There isn't name or it has wrong format:$a->{num}_$a->{name}\n";}
        if (exists $a->{num} && exists $a->{name}) {
		foreach my $b(@checknum){
			print STDERR "ERROR: duplicate number found: $b->{num}_$b->{name} and $a->{num}_$a->{name}\n" if ($b->{num} eq $a->{num});
			print STDERR "ERROR: duplicate name found: $b->{num}_$b->{name} and $a->{num}_$a->{name}\n" if ($b->{name} eq $a->{name});
		}
		push @checknum, $a;
	}
}