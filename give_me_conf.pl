use 5.010;
use strict;
use warnings;
use Data::Dumper;

my $mode = $ARGV[0] || "nginxsmall"; # rtmppublishhls nginxsmall
#my $grepwarn = $ARGV[1] || "1";
     
my $num = 'last'; #' last ' or number
my $name ='fxclub';#получать из аргументов
my $numname = $num. "_". $name;
my $date = getDate();#нолик в конце

my $lastnum=checkDublicateAndCorrectFormat();
say "ИСПРАВИТЬ АБСОЛЮТНЫЕ АДРЕСА /nginx/template на дефаул";
#####wowza######
my $pass = getPass();
my $stream = $name. ".sdp";
my $player_name_slon = $name. "_slon";
my $player_name_jw_flow = $name. "_jw_flow";

#####NGINX######
my $cache_size = "100m";
my $domain = "libertex-std.fxclub.org";
my $cache_valid = "4h";#h-часы d-дни
my @ar_cnames = ('lxcdn-std.fc-static.com');
my @ar_testfiles = ('/images/global/login/android-small.jpg'); #убирать домены

my $commandsfortestfiles = getCommandsForTestFilesNginx();
my $cnames = join(' ', @ar_cnames) ;

################

$num = ++$lastnum if $num == 'last';

if($mode eq "rtmppublishhls"){
my $textArray = qq[
##########WOWZA rtmppublishhls##########
,{ num => '$num'
        , name => '$name'
                , date_config => '$date'
                , wowza => 'true'
                , wowza_ip_template => '10003_template03'
                , wowza_live_param => {
                        publish_points => [
                        {
                                name => 'pub1.rtmp.s01.l.$name',
                                alias => 'pub-04'
                        },
                        {
                                name => 'pub2.rtmp.s01.l.$name',
                                alias => 'pub-01'
                        },
                        ]
                                , send_customer => "true"
                }
        , 'wowza_publish_app' => {
                app => [
                {
                        id => '1'
                                , type => 'RTMP-Publish' # RTMP-Publish or RTMP-Pull or RTSP-Pull or MpegTS
                                , app_symlink => '$name' # symlink on app
                                , publish_auth => { # for type RTMP-Publish
                                        type => 'stream_auth' # adobe or param or stream_auth
                                                , stream_auth_list => [ {stream => '*' , pass => '$pass'}]
                                }
                        , cupertino => 'true' # default false
                        , sanjose => 'false'
                        , http_origin_mode => { # false if DVR to true
                                        playlist_maxage => '0' # default 0 sec
                                        , cupertino_playlist_maxage => '0' #high priority
                                }
                }
                ]
        }
 , 'wowza_edge_app' => {
                app => [
                {
                        id => '1'
                                , app_symlink => '$name' # symlink on app
                                , route => {
                                        origin_scheme => 'aliasmap' # aliasmap or other modules
                                                , origin_app => '$name' # name app on publish servers
                                                , aliasmap_servers_list => [ 'pub1.rtmp.s01.l.$name', 'pub2.rtmp.s01.l.$name' ]
                                }
                }
                ]
        }
 , player => [
        {
                type => 'slon'
                        , name => '$player_name_slon'
                        , stream => [{name => '$stream'}]
                        , url => 'rtmp://$name.cdnvideo.ru/$name'
        }
        ,{
                type => 'standard'
                        , name => '$player_name_jw_flow'
                        , stream => [{name => '$stream'}]
                        , url => 'rtmp://$name.cdnvideo.ru/$name'
        }
        ]

                , ip => {
                        dns => [ '$name' ]
                }
       , name_www => '$name'
                , nginx => 'true'
                , nginx2 => 'true'
                , nginx_cache_size => '100m'
                , nginx_ip_template => '10001_template01'
                , nginx2_ip_template => '10004_template04'
                , nginx_routes => '00000_default.rt'
 , nginx_upstream =>
                [
                {
                        id => '1'
                                , domain => 'pub1.rtmp.s01.l.$name.cdnvideo.ru:1935'
                                , backup => 'pub2.rtmp.s01.l.$name.cdnvideo.ru:1935'
                                , midorig => { port => '3127' }
                        , alias => 'live.$name.cdnvideo.ru *.live.$name.cdnvideo.ru *.live.$name live.$name pub1.rtmp.s01.l.$name.cdnvideo.ru pub2.rtmp.s01.l.$name.cdnvideo.ru'
                                , proxy_cache_valid => '300s'
                }
        ]
                , ip_www =>
                {
                        dns => [ 't.$name']
                }
                , ip_www2 =>
                {
                        dns => [ 'live.$name']
                }
}

___________________________________
Проверить есть ли уже такое имя. Проверить дату. Проверить номер.
Проверить все ли плееры нужны, если слон нужен, то исправить ссылку в нем на HLS.
Сделать и закоммитить ссылку на default+routes:
ln -s  /etc/puppet/modules/nginx/templates/00000_default+routes.conf  /etc/puppet/modules/nginx/templates/$numname.conf
Проверить 
ls /etc/puppet/modules/nginx/templates/$num*
Занести в cust.pl, сделать проверку cdn.pl, сделать cdn.pl bind.
Поменять пароль в типовом, если есть занести jw flow.	
/usr/local/bin/wowza_check.pl $name $name $name.sdp
/usr/local/bin/nginx_check.pl live.$name /$name/$name.sdp/playlist.m3u8

Еще раз проверить имя, номер, дату и пароль.

Ссылки публикации:
FMS URL primary: rtmp://pub1.rtmp.s01.l.$name.cdnvideo.ru/$name
FMS URL backup: rtmp://pub2.rtmp.s01.l.$name.cdnvideo.ru/$name
(бэкап следует использовать только если достаточно полосы для публикации двух потоков)
Stream: $name.sdp?auth=$pass

Ссылки просмотра:
Тестовый Flash-плеер: http://www.aloha.cdnvideo.ru/aloha/wowza-examples/LiveVideoStreaming/client/live.html
Ссылка для Flash-плеера:
ConnectURL: rtmp://$name.cdnvideo.ru/$name
Stream: $name.sdp

Flash-плеер c embed кодом для встраивания:
http://www.aloha.cdnvideo.ru/aloha/players/$player_name_slon.html

HLS: http://live.$name.cdnvideo.ru/$name/$name.sdp/playlist.m3u8
RTMP: rtmp://$name.cdnvideo.ru/$name/$name.sdp
	];
	say $textArray;
}	

elsif($mode eq "nginxsmall"){
my $textArray = qq[
##########NGINX HTTP-CACHING##########
===============AFFSMALL===============
,{ num => '$num'
        , name => '$name'
                , date_config => '$date'
                , nginx => 'true'
                , nginx_cache_size => '1g'
                , nginx_ip_template => '10001_template01'
                , nginx_routes => '00000_small.rt'
                , nginx_upstream => [
                {
                        id => '1'
                                , domain => '$domain'
                                , alias => '$name.cdnvideo.ru *.$name.cdnvideo.ru *.$name $name $name.ru $cnames'
                                , proxy_cache_valid => '$cache_valid'
                                , mp4_streaming => 'false'
                                , flv_streaming => 'false'
                }
        ]
                ,ip => {
                        dns => [ '$name' ]
                                , balancer => "cdnvideo.net"
                                , balancer2 =>  {
                                        suffix => "cdnvideo.net",
                                               dns => [ '$name' ]
                                }
                        , affinity => { 'AFFSMALL' => '*', }
                }
}
_____________________________________
Проверить есть ли уже такое имя. Проверить дату. Проверить номер.
Сделать и закоммитить ссылку на default+routes:
ln -s  /etc/puppet/modules/nginx/templates/00000_default+routes.conf  /etc/puppet/modules/nginx/templates/$numname.conf
Проверить 
ls /etc/puppet/modules/nginx/templates/$num*
Занести в cust.pl, сделать проверку cdn.pl, сделать cdn.pl bind.
$commandsfortestfiles
Пока в тестовом режиме.

###ИСПРАВИТЬ
Еще раз проверить имя, номер, дату и пароль.

Для Вас настроен домен $name.cdnvideo.ru (разрешенные CNAME: $cnames)
HTTP-запросы к этому домену проксируются на $domain.
Ответы кэшируются на узлах CDN.
При кэшировании учитываются HTTP заголовки Expires: и Cache-Control:
Если заголовки не указаны, время кэширования $cache_valid.
Размер кэш = $cache_size.
	];
	say $textArray;
}

  


 
 
 
 
 
 #functions
 
  sub getDate #постестить
{
                my @date_array = localtime();
                my $year = $date_array[5]+1900;
                my $mon = ++$date_array[4];
                my $day = $date_array[3];
               $mon ="0". $mon if($mon < 10);
                 $day ="0". $day if($day < 10);
                my $date = $year. "-". $mon. "-". $day;
                return $date;
}

sub getPass #убрать специальные символы в генерации паролей
{
my $len = 8;
my @chars = ("a" .. "z", "A" .. "Z", 2 .. 9);
my $randpass = join("", @chars[ map { rand @chars } (0 .. $len) ]);

	 my @date_array = localtime();#
                my $year = $date_array[5]+1900;
                my $mon = ++$date_array[4];
                $mon ="0". $mon if($mon < 10);
	my $pass=$year. $mon. $date_array[3].$randpass;
	#for (1..$len) {$pass .= chr(rand(94)+33)}
return $randpass;
}

sub addNull #в getDate добавить в $mon
{
}
	 
sub getCommandsForTestFilesNginx{
        my $commandsfortestfiles;
        foreach(@ar_testfiles){ 
                $commandsfortestfiles.= "/usr/local/bin/nginx_check.pl $name $_ \n"
        }
        return $commandsfortestfiles;
}	 

sub checkDublicateAndCorrectFormat
{##trycatch добавить стоитsay "GREP NAME";
	unless($num =~ /^\d{5}$/ || $num == 'last') {say "ERROR: number has wrong format:$num";}
	if(!$name =~ /^\w+$/ || $name =~ /[-\/]/) {say "ERROR: name has wrong format:$name\n";}
	        
say "LAST CLIENT:";
say my $lastclientstr = `ssh b05 grep -A 1 num /etc/puppet/cust.pl | tail -n 2`;
say "asdasd". $lastclientstr;
say "GREP NAME";
say my $chkgrepstringname = `ssh b05 grep -A1 $name /etc/puppet/cust.pl`;
say "GREP NUM";
say my $chkgrepstringnum = `ssh b05 grep -A1 $num /etc/puppet/cust.pl`;  
say "LASTNUM:";
say my ($lastnum)=$lastclientstr =~m/\d{5}/g; 
return $lastnum;      
}